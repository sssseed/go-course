# 项目课


## 项目介绍

### Web全栈开发: 微博客

+ 项目名称: vblog, 一套前后端分离的单体Web服务
+ 适合对象: 新手项目入门
+ 业务方向: Web全栈开发, 运维开发
+ 项目数量: 2个(后端+前端)
+ 难度: 中
+ 教学方式: 全程手写代码
+ 项目关键技术:
    1. 使用Gin框架开发Restful API
    2. 使用Gorm来与MySQL进行交互
    3. OOP思想在项目的运用
    4. 面向接口与面向组合思想在项目中的应用
    5. 使用TDD方式开发业务模块
    6. 使用ioc解决对象依赖问题
    7. 业务Debug技巧
    8. 自定义业务异常
    9. API返回数据的统一处理
    10. 面向业务分区的统一架构
    11. 基于Token的简化版 RBAC认证
    12. 配置文件同时支持Env和配置文件
    13. Web基础与前端布局技术从0到1实战
    14. 前端框架采用Vue3开发
    15. UI组件库采用 ArcoDesign
    16. Restful API客户端 SDK开发
    
#### 架构设计

![](./image/projects/vblog-arch.png)


#### UI展示

![](./image/projects/vblog_login.png)

![](./image/projects/frontend-blog-list.png)

![](./image/projects/backend-blog-list.png)

![](./image/projects/blog-edit.png)


### 微服务开发: 微服务研发云迷你版

+ 项目名称: devcloud-mini
+ 适合对象: 有基本的单体项目开发经验，想深入进阶微服务开发的同学
+ 业务方向: 大型分布式业务系统研发, 业务中台, 比较低层服务(上层的业务是根据市场变化), 业务中间件(网关)
+ 项目数量: 3个
+ 难度: 中高
+ 教学方式: 后端代码部分手写 + 前端代码提供样例
+ 项目关键技术:
    1. 使用Go Workspace 进行微服务多模块开发
    2. 外部调用接口 采用goRestful 开发
    3. 内部调用接口 采用GRPC 开发
    4. 采用kafka进行异步消息通讯
    5. 链路追踪: opentelemetry 接入Trace
    6. 应用自定义监控: 使用prometheus SDK开发Exporter
    7. 如何使用ioc托管配置对象
    8. GRPC客户端开发
    9. Restful API 和 Grpc API中间件开发
    10. 使用Makefile作为项目脚手架


#### 微服务用户与权限中心

##### 架构设计

![](./image/mcenter/mcenter_auth.png)

![](./image/mcenter/mcenter_perm.png)

##### UI展示

+ 登录页面

![](./image/projects/devcloud-login.png)

+ 管理后台

![](./image/projects/devcloud-ucenter-user.png)
![](./image/projects/devcloud-ucenter-role.png)




#### 微服务审计

##### 架构设计

![](./image/maudit/maudit.png)

##### UI展示

![](./image/projects/devcloud-ucenter-audit.png)

#### CMDB

##### 架构设计

![](./image/cmdb/cmdb.png)


##### UI展示

![](./image/cmdb/cmdb-ui.png)


### 复杂业务开发解读: 微服务研发云平台: 流水线项目

+ 项目名称: devcloud
+ 适合对象: 熟悉微服务开发模式, 了解复杂业务开发流程, 想要从事平台开发业务方向的同学
+ 业务方向: 平台开发
+ 项目数量: 2个
+ 难度: 高
+ 教学方式: 代码解读与演示

#### 发布中心

基于k8s的微服务发布平台, 基础功能:
+ 多个Kubernetes集群管理
+ 服务的部署与创建
+ 服务日志查看
+ 服务控制台登录

##### 架构设计


##### UI展示

![](./image/projects/devcloud-mpaas-service.png)
![](./image/projects/devcloud-mpaas-deploy.png)
![](./image/projects/devcloud-mpaas-log.png)
![](./image/projects/devcloud-mpaas-console.png)


#### 流水线编排

基于k8s job设计的 流水线服务, 基础功能:
+ Job的管理
+ Job执行与调试
+ Pipeline的管理
+ Pipeline的执行与调试
+ Pipeline 触发器管理(与Gitlab联动)

##### 架构设计

![](./image/mflow/mflow.png)

##### UI展示

![](./image/projects/devcloud-mflow-job-edit.png)
![](./image/projects/devcloud-mflow-job-run.png)
![](./image/projects/devcloud-mflow-pipeline.png)
![](./image/projects/devcloud-mflow-pipeline-job.png)

## 课程大纲

### Web全栈开发

#### Vblog项目介绍与骨架搭建

1. 项目概要设计: 需求,流程,原型与架构
2. 项目详细设计: API接口设计,数据库表结构设计
3. 业务代码组织流程设计(面向对象的程序设计)
4. 项目架构风格: 功能分区与业务分区架构(面向业务DDD简化架构)

`详细说明`:
1. 项目课整体介绍
2. 软件开发生命周期流程介绍
3. 项目需求概述: Markdown博客管理系统
4. 项目原型草图: 访客前提与博客管理后台
5. 项目技术栈与架构: Gin+GROM+Vue3+ArcoDesign
6. 项目RESTful风格API设计
7. 项目中的单元测试与Vscode单元测试配置
8. Vscode中如何对Go项目进行单步调试
9. 如何编写功能分区架构风格的代码(MVC)
10. 如果编写业务分区架构风格的代码(DDD)

#### Vblog项目V1简化版开发: 采用业务分区模式+TDD模式开发用户管理模块

1. 用户管理模块的业务定义(面向接口)
2. 如何管理项目的配置
3. 用户管理模块接口实现与测试
4. 基于Bcrypt处理用户密码存储问题

`详细说明`:
1. 面向接口与业务分区模型下的应用实践
2. 使用接口来定义用户管理模块的业务领域接口
3. 枚举对象的应用场景以及如何定义
4. 如何设计程序的配置对象
5. 如何基于配置对象构造单例模式的数据库连接池对象(GROM DB对象)
6. 业务控制器如何处理对配置对象的依赖
7. 配置对象的加载(支持环境变量与配置文件)
8. 为配置对象编写单元测试
9. 设计用户管理模块的业务领域接口的具体实现
10. 为领域接口的实现 编写单元测试
11. 领域接口的实现类(具体实现) 实现用户创建方法
13. 使用单元测试验证实现类实现的用户创建方法是否正确

#### Vblog项目V1简化版开发: 登录管理模块开发

1. 认证流程设计与令牌管理服务的接口定义
2. 令牌管理服务的服务实现与测试
3. 利用Gin框架实现Restful风格的登录接口开发
4. 通过main组装好程序并使用Postman进行接口测试

`详细说明`:
1. API认证中基于Token的令牌颁发机制
2. Token令牌颁发模块领域接口定义
3. 令牌过期机制设计与令牌刷新
4. 令牌对象格式化之实现fmt Stringer接口
5. 令牌接口之 登录/退出/校验
6. 接口定义中Context的作用
7. 设计Token管理模块的业务领域接口的具体实现
8. 为领域接口的实现 编写单元测试
9. 领域接口的实现类(具体实现) 实现登录/退出/校验
10. 使用单元测试验证实现类实现的登录/退出/校验方法是否正确
11. 设计Token管理模块的RESTful风格HTTP API
12. 设计Token管理模块的 API Handler处理类
13. Gin框架的请求产生获取/处理/与响应
14. 业务处理中的自定义业务异常实现(ApiException)
15. API数据返回结构(Response)统一定义
16. 通过Main组装并启动程序

#### Vblog基于Ioc优化程序依赖管理

1. 对象依赖管理与Ioc思想
2. 实现一个简单的ioc库
3. 基于Ioc重构控制器与API Handler
4. 为程序添加CLI

`详细说明`:
1. 对象强依赖下的手动管理问题
2. 依赖倒置思想是如何解耦对象强依赖问题
3. 如何设计一个Ioc容器，实现对象的注册和获取
4. Ioc容器对象的生命周期: 注册，初始化, 获取，销毁
5. 使用namespace来进行 不同对象的分类管理(API/Controller/Default)
6. Ioc容器接口定义
7. 基于map实现一个基础版本的Ioc容器
8. Ioc与面向接口编程如何结合使用: 对象断言
9. 什么是切面编程，Ioc容器利用切面实现 API Handler自动注册
10. 使用Ioc改造 user模块, 托管User业务服务的 实现类
11. 使用Ioc改造 token模块, Token模块通过Ioc获取依赖模块(User模块)
12. 使用Ioc改造 token模块, 托管Token模块API Handler, 并从Ioc中获取Token服务具体实现
13. 改造程序入口, 初始化Ioc, 并启动程序

#### Vblog基于Ioc开发博客管理模块

1. 博客管理模块的接口定义与实现
2. 为博客管理模块添加Restful接口
3. API认证中间件开发与使用
4. 工程优化: 优雅关闭与Makefile脚手架

`详细说明`:
1. 博客管理模块需求分析: 博客的增删改查
2. 博客管理模块领域接口定义: 数据结构与接口
3. 博客管理模块领域接口实现: TDD模式开发
4. 博客创建功能实现: 参数检测, 对象构造, 与入库
5. 博客列表查询功能: 分页查询, 条件过滤，关键子过滤，数据二次加工
6. 博客详情查询功能: 如何处理NotFound
7. 博客详情更新功能: 全量更新(Update)与部分更新(Patch)的实现
8. 博客单个删除功能: 设计完善的接口, 不允许重复删除
9. 博客管理模块RESTful API接口开发: 连通接口与业务控制
10. 如何通过Ctx处理接口请求取消
11. 博客管理模块RESTful API接口权限设计: Gin中间件的使用
12. 开发RESTful API接口权限认证中间件
13. RESTful API接口应用权限中间件
14. 应用中间件后,如何从中间件中获取用户身份信息
15. 使用scope控制数据访问范围
16. 使用Postman对开发完成的接口进行测试
17. 优雅关闭原理，以及如何基于信号量处理优雅关闭
18. 为你程序补充Makefile, 执行make run快速启动

#### Web入门

1. JavaScript基础
2. HTML基础入门
3. CSS基础入门
4. 浏览器基础

`JavaScript基础 详细说明`:
1. 前端环境准备: Node安装, 国内源切换, Vscode插件配置
2. 基础数据类型介绍: Number/字符串/布尔值/数组/对象
3. 变量声明与使用以及null 与 undefine的区别
4. 字符串的常用操作: 转义/模版/拼接/多行字符串/大小写
5. try catch模式的异常处理: 异常类型/异常抛出/异常捕获
6. 函数，方法，匿名函数以及箭头函数的使用
7. JS中的模块化机制
    1. 全局作用域与window
    2. 使用export进行具名导出
    3. 使用import进行具名导入已经别名
    4. 基于命名空间的具名导出
    5. 使用export default进行默认导出
    6. 使用import引入默认导出
8. JS的条件判断(IF语句)
9. For循环介绍: for原始语法, for in/ for of/forEach
10. JS中的异步编程: Promise对象
    1. 单线程异步模型
    2. Promise与异步
    3. Async函数 + Promise组合

`HTML/CSS/浏览器 详细说明`:
1. HTML 网页结构
2. HTML 标签与元素
3. HTML 元素语法
4. 常用标签介绍
5. CSS语法
6. CSS选择器: 基础选择器/层级选择器/属性选择器/伪类选择器
7. CSS引入方式: 内联/内嵌/外联
8. CSS样式基础: CSS单位/CSS颜色
9. CSS创建属性: 元素尺寸控制/盒子模型/Overflow
10. 浏览器对象介绍
11. 浏览器AJAX异步网络请求介绍
12. 如何使用JS操作DOM: DOM的增删改查

#### Vue3入门

1. Vue初体验
2. 前端发展史与MVVM思想的诞生
3. Vue实例与生命周期
4. Vue响应式原理与模版语法

`详细说明`:
1. 环境准备: Vue Devtools与Vscode Vue3语法插件
2. Vue项目初始化与工程目录结构解读
3. Vue与传统HTML/CSS/JS的关系以及Vue项目的部署
4. MVVM诞生历史背景介绍
5. Vue与MVVM的关系
6. Vue中的项式 API和组合式 API
7. Vue实例以及实例的声明周期
8. Vue响应式原理:
    1. JavaScript Proxy
    2. 属性劫持: getter/setters
    3. setup 语法糖
    4. DOM异步更新
    5. 深层响应性
    6. 如何自定义数据侦听
9. Vue模版语法: 文本/表达式/计算属性
10. 响应式绑定:
    1. 元素属性绑定
    2. 元素事件绑定
    3. 元素样式绑定(Class/Style)
    4. 表单输入绑定
    5. 自定义指令
11. 条件渲染与列表渲染

#### Vue进阶与Vblog登录页面

1. Vue组件基础
2. VueUI组件库与Arco Design的基本使用
3. Vblog工程初始化
4. 登录页面开发

`详细说明`:
1. 组件工作原理
2. 组件的定义/使用/注册
3. 动态组件
4. 内置组建
5. 组件通信
    1. 传递 props
    2. 监听事件
    3. 双向绑定
    4. 依赖注入
6. 组合式函数
    1. 组合式函数诞生的场景
    2. 定义组合式函数
    3. 使用组合式函数
    4. 组合式函数开源市场: VueUse库
7. vue插件, 以及Vue3 UI插件介绍
8. Vblog工程初始化
9. Arco Design插件安装
10. 登录页面布局设计, 使用Flex剧中登录框
11. 使用Arco Design Form表单组件 开发登录框
12. 使用Arco Design Input组件接收用户参数
13. Form表达验证, 用户参数必填与password秘密至少6位
14. 使用axios发送ajax请求, 封装client, 构造登录API调用
15. axios 拦截器统一处理页面请求数据(成功与失败)

#### Vblog后台管理页面开发

1. 后台Layout布局
2. 顶部导航TopBar组件开发
3. 后台博客管理页面开发
4. 页面访问保护(导航守卫)

`详细说明`:
1. 如何利用vue router的路由嵌套来完成页面Layout布局
2. 前后台页面分析, 拆解出共用组件(顶部导航): TopBar
3. 浏览器存储功能介绍: cookie/session storage/local storage
4. 采用local storage保持用户的持久化用户登录状态
5. 通过useStorage使用响应式的local storage对象
6. TopBar开发:
    1. 判断当前处于前台还是后台
    2. 判断用户当前是登录还是未登录
    3. 处于前台并且已经登录才显示后台
    4. 登录时保持状态, 退出时清理状态
7. 使用Arco Design Menu组件开发侧边栏导航
8. 文章管理列表页开发:
    1. 使用Arco Design Breadcrumb组件添加列表页页头 博客管理/文章管理
    2. 使用Arco Design Table组件显示文章列表
    3. 使用Arco Design Pagination 适配服务端分页
    4. 使用Dayjs处理时间戳的显示
    5. 列表页 实现关键字过滤
    6. 使用Arco Design Form组件和md-editor-v3组件开发文章创建与编辑页面
    7. 使用Arco Design Popconfirm组件 开发删除提醒


### 微服务开发之Devcloud

#### 微服务基础之 RPC与Protobuf基础

1. Go语音内置RPC框架的使用
2. 基于接口封装优化好的RPC
3. Protobuf介绍与环境准备
4. Protobuf编解码与语法介绍

`详细说明`:
1. RPC使用场景以及概述
2. Go语言内置RPC框架的基本使用
    1. RPC 服务端开发
    2. RPC 客户端开发
    3. 测试客户端调用服务端
3. 基于接口来优化Go语言内置的RPC服务
4. Go编码与跨语言RPC
5. Go语言内置RPC实现JSON ON TCP
6. Go语言内置RPC实现JSON ON HTTP
7. Protobuf概述
8. Protobuf环境搭建与Go语言插件安装
9. Protobuf 快速体验: 使用Protobuf生成Go语言代码
10. Protobuf 语法详解:
    1. 定义消息类型
    2. Protobuf类型与Go语言类型对应关系
    3. 枚举类型/数组类型/Map等基础类型的使用
    4. 消息嵌套与限制
    5. Protobuf的包机制以及使用方式
    6. 使用限制与更新规范

#### 微服务基础之 GRPC与Ioc融入

1. GRPC入门之客户端与服务端
2. GRPC服务端实例类融入Ioc框架
3. 基于Grpc开发Vblog评论模块
4. GRPC中间件

`详细说明`:
1. 跨语言RPC的场景以及GRPC技术栈介绍
2. Hello gRPC
    1. protobuf grpc插件
    2. 定义服务接口并生成代码
    3. grpc服务端代码编写
    4. grpc客户端代码编写
3. gRPC Stream
    1. 定义 双向流 的GRPC服务
    2. 双向流服务端实现
    3. 双向流客户端实现
4. GRPC服务端实例类融入Ioc框架
    1. 定义GrpcHandler类型接口，识别对象的GRPC接口特征
    2. IOC完成GRPC控制器对象的自动注册
    3. 启动GRPC服务监听
5. 基于Grpc开发Vblog评论模块
    1. 评论模块需求梳理
    2. 评论模块RPC定义
    3. 为Protobuf Message添加自定义Tag
    4. 评论模块RPC内部接口定义
    5. 评论模块服务端类实现
    6. 基于GRPC编写客户端SDK并测试
6. GRPC中间件
    1. GRPC中间件原理
    2. 基于GRPC中间件实现基于clientID和ClientKey的认证介绍
    3. 编写认证中间件(Request Response模式)
    4. Server添加认证
    5. Client携带认证
    6. 验证客户端认证消息

#### 微服务Devcloud研发平台开发: 用户中心-中心化认证

1. 微服务多工程项目组织方式介绍
2. DevCloud需求功能与架构设计
3. 用户中心 认证服务端与客户端开发
4. 用户中心 认证接入中间件开发

`详细说明`:
1. 基于Golang的Workspace组织微服务工程
2. 如何从单体项目中抽象通用模块
3. 基于抽象好的通用模块(二方库)编写一个样例项目
    1. 编写helloworld业务模块
    2. 注册编写好的业务模块(Controller和API)
    3. 程序入口文件, 启动应用程序
    4. 补充应用程序配置，包含MongoDB依赖注入
4. DevCloud需求功能与架构设计
5. 用户中心需求分析
    1. 中心化认证
    2. 中心化鉴权
6. 中心化认证和鉴权的方案与架构
7. 用户中心技术栈介绍: GoRestful + MongoDB + Grpc
8. 环境准备(主要是MongoDB以及UI工具)
9. 引用用户认证相关所有模块(用户管理，Token颁发等)
10. 本地User/Password 验证流程解读:
    1. Domain的管理
    2. 子账号管理(domain下面的账号)
    3. 基于本地的令牌的颁发
11. 集成飞书二维码登录验证流程解读:
    1. 飞书认证流程解读
    2. 准备一个企业账号
    3. 创建一个企业自建应用
    4. 准备一个飞书登录集成网页
    5. 开发飞书登录集成服务端程序
    6. 根据返回的code做飞书登录测试

#### 微服务Devcloud研发平台开发: 用户中心-中心化鉴权

1. 认证中间件开发与接入
2. 通用RBAC鉴权流程设计
3. 用户中心 权限服务端与客户端开发
4. 认证中间件补充鉴权逻辑

`详细说明`:
1. 基于中间件的接入架构设计
2. 基于用户中心GRPC客户端开发认证中间件
3. 初始化一个样例服务(cmdb) 编写一个secret管理模块, 用于验证接入测试
4. 测试cmdb服务能否使用认证中间件接入用户中心进行统一认证
5. RBAC与PBAC的鉴权模型解读
6. 基于PBAC鉴权模型设计以及鉴权期望
7. 引用用户鉴权相关所有模块
8. 用户鉴权流程解读:
    1. 接入中心化鉴权的服务(cmdb)注册功能列表
    2. 创建权限测试角色 管理员/访客
    3. 创建2个测试用户  admin01/visotor01
    4. 为2个测试用户 添加授权策略
    5. 验证鉴权功能: 分别校验admin01/visotor01用户是否有创建secret的权限
9. 利用RPC客户端 封装鉴权逻辑, 中间件补充鉴权
10. 更新最新的接入中间件, 测试cmdb 接入用户中心后的鉴权效果

#### 微服务Devcloud研发平台开发: CMDB设计与凭证管理

1. 常见的CMDB设计模式
2. 类云管CMDB设计方案与流程
3. 资源管理模块开发 
4. 云商凭证管理模块开发

`详细说明`:
1. CMDB需求介绍
2. 常见的CMDB设计模式
    1. 基于模型的CMDB系统设计
    2. 基于关系型数据的直接设计
    3. 2中模式的优劣
3. CMDB模块划分与业务流程设计
4. 资源管理模块开发
    1. 资源模块功能接口定义(资源入库与搜索)
    2. 基于MongoDB实现接口
    3. 添加RESTful API
    4. 装载模块, 测试 RESTful API/GRPC API
5. 云商账号管理模块功能开发
    1. 云凭证管理模块功能接口定义
    2. 基于MongoDB实现接口
    3. 添加RESTful API
    4. 装载模块, 测试 RESTful API/GRPC API
    5. 敏感信息存储如何加解密
    6. 敏感信息如何脱敏

#### 微服务Devcloud研发平台开发: CMDB资源同步与审计中心

1. 开发腾讯云VM资源同步Provider
2. Secret模块集合Provider实现云资源同步
3. 消息队列与Kafka
4. 审计中心基于消息队列模式的设计与实现

`详细说明`:
1. 资源同步器开发
    1. 资源同步器接口定义: 统一各个云厂商对资源定义的差异
    2. 云账号环境准备
    3. 云商API文档与SDK文档的使用
    4. 腾讯云CVM SDK 测试
    5. 开发腾讯云CVM资源同步器
2. 云商账号资源同步功能开发
3. 消息队列介绍介绍
    1. 什么是消息队列以及消息队列的使用场景
    2. 消息队列核心概念介绍
    3. 消息模式
        1. 点对点模型
        2. 发布订阅模型
4. 典型消息队列服务kafka简介
    1. Topic与Partition
    2. 数据冗余方案
    3. 消费组
    4. 如何保证消息的顺序性
    5. 集群状态维护
5. 基于kafka的消息队列实战
    1. 环境准备
    2. kafka Go SDK选择
    3. 创建Topic
    4. 生产者(Producer) 发送消息
    5. 消费者(Consumer) 接收消息
    6. 认证
6. 审计中心基于消息队列模式的设计与实现

#### 微服务工程基础: 应用自定义监控与链路追踪

1. Prometheus 概念介绍和Exporter 开发基础
2. 审计中心 基于Exporter模式 实现自定义监控
3. 基于OpenTelemery链路追踪技术介绍
4. cmdb接入Trace 实战

`详细说明`:
1. Prometheus 概念介绍
2. Exporter 开发基础
3. 审计中心 基于Exporter模式 实现自定义监控
4. 基于OpenTelemery链路追踪技术介绍
5. cmdb接入Trace 实战
    1. http 接口开启Trace
    2. grpc 接口开启Trace
    3. mongodb 开启 trace
    4. 访问cmdb secret接口 查询联络调用信息

### 基于K8s Operator的 CI CD项目解读

#### 自研CICD平台代码解读: mpaas与mflow

1. Kubernetes 简介与client-go使用
2. 基于kubeconf设计多集群管理系统
3. 使用k8s job的流水线设计方案解读
4. Docker Build Job执行演示

`详细说明`:
1. Kubernetes 简介
2. client-go使用
    1. 如何基于kubeconf进行认证
    2. 基于client-go完成对Deployment资源的操作
    3. 基于client-go实现服务的日志控制台
3. mpaas 发布平台 多集群设计方案解读
4. mpaas 发布平台 服务发布演示与解读
    1. 使用gitlab private token同步服务
    2. 录入k8s管理凭证
    3. 通过 API 完成服务的部署
    4. 控制台查看服务日志
    5. 登录服务控制台
5. mflow 流水线平台 设计方案解读
6. 基于kaniko的容器构建job创建与执行演示

#### 自研CICD平台代码解读: k8s operator开发

1. Pipeline流程代码解读
2. Gitlab触发流程代码解读
3. k8s operator 开发模式解读

`详细说明`:
1. pipeline 流程介绍
2. Gitlab Hook机制介绍
3. 对接Gitlab Hook完成流水线的触发
4. k8s operator 原理介绍
5. k8s operator 项目moperate解读
6. k8s operator Demo实战
